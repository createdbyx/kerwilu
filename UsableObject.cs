/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System;

    using CBX.Kerwilu.Args;
    using CBX.Kerwilu.Interfaces;
                        

    /// <summary>
    /// Provides a base implementation for the <see cref="IUseable"/> interface.
    /// </summary>
    public class UsableObject : IUseable
    {
        #region Constants and Fields

        /// <summary>
        /// Holds onto a permanent reference to a <see cref="UsageEventArgs"/> type.
        /// </summary>
        private readonly UsageEventArgs args = new UsageEventArgs();

        /// <summary>
        /// Holds a reference to a <see cref="IEventService"/> type.
        /// </summary>
        private readonly IEventService events;

        /// <summary>
        /// Holds the reference to the cool down time.
        /// </summary>
        private TimeSpan cooldownTimeSpan;

        /// <summary>
        /// Holds a value whether this is the first update.
        /// </summary>
        /// <remarks>The Update methods uses this value to determine if it's the first time being called after the usage object has started.</remarks>
        private bool firstUpdate;

        /// <summary>
        /// Hold a value to determine if this usage object is in use.
        /// </summary>
        private bool inUse;

        /// <summary>
        /// Value to determine if usable object is off cool down.
        /// </summary>
        private bool isOffCooldown = true;

        /// <summary>
        /// Used to store the last cool down time.
        /// </summary>
        private TimeSpan lastCooldownTime;

        /// <summary>
        /// Holds a reference to the last time Update was called.
        /// </summary>
        private TimeSpan lastTime;

        /// <summary>
        /// Used to hold the remaining time before the item can be used again. 
        /// </summary>
        private TimeSpan remainingTimeOnCooldown = TimeSpan.Zero;

        /// <summary>
        /// Holds the time that this usage will be alive for.
        /// </summary>
        private TimeSpan usageDuration = TimeSpan.Zero;

        /// <summary>
        /// Holds the number of times it can be used.
        /// </summary>
        private int usageLimit = int.MinValue;

        /// <summary>
        /// Used to log the usage time that this usage object has been in use for.
        /// </summary>
        private TimeSpan usageTimeSpan;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UsableObject"/> class.
        /// </summary>
        /// <param name="service">
        /// A reference to a <see cref="IEventService"/> service type.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If service is null.
        /// </exception>
        public UsableObject(IEventService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }

            this.events = service;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// Event that gets raised when usage begins.
        /// </summary>
        public event EventHandler<UsageEventArgs> StartUsage;

        /// <summary>
        /// Event that gets raised when usage is over.
        /// </summary>
        public event EventHandler<UsageEventArgs> StopUsage;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the number of charges left for the item.
        /// </summary>
        /// <returns>Returns the number of remaining charges. If the values is int.MinValue then there is no limit on it's use.</returns>
        public virtual int Charges
        {
            get
            {
                return this.usageLimit == int.MinValue ? int.MinValue : this.usageLimit;
            }
        }

        /// <summary>
        /// Gets the cool down time before the item can be used again.
        /// </summary>
        public virtual TimeSpan Cooldown
        {
            get
            {
                return this.cooldownTimeSpan;
            }
        }

        /// <summary>
        /// Gets the remaining time before the item can be used again.
        /// </summary>
        public virtual TimeSpan RemainingTimeOnCooldown
        {
            get
            {
                return this.remainingTimeOnCooldown;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the description for the usable implementation.
        /// </summary>
        /// <returns>
        /// Returns a description string.
        /// </returns>
        public virtual string GetDescription()
        {
            var manager = Localization.LocalizationManager.Instance;
            int charges = this.Charges;
            string chargeString = charges >= 0 ? string.Format("\n( {0}: ", manager.Get("Charges")) + charges + " )" : string.Empty;

            string cooldownString = this.cooldownTimeSpan > TimeSpan.Zero ? " ( " + this.Cooldown.ToCooldownString() + string.Format(" {0} )", manager.Get("cooldown")) : string.Empty;
            if (this.isOffCooldown == false)
            {
                cooldownString = " ( " + this.RemainingTimeOnCooldown.ToCooldownString() + string.Format(" {0} )", manager.Get("remaining"));
            }

            return chargeString + cooldownString;
        }

        /// <summary>
        /// Sets the number of charges.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public virtual void SetCharges(int value)
        {
            this.usageLimit = value;
        }

        /// <summary>
        /// Sets the cool down time.
        /// </summary>
        /// <param name="value">
        /// The value to set cool down to.
        /// </param>
        public virtual void SetCooldown(TimeSpan value)
        {
            this.cooldownTimeSpan = value;
            if (this.isOffCooldown)
            {
                this.remainingTimeOnCooldown = this.cooldownTimeSpan;
            }
        }

        /// <summary>
        /// The set duration.
        /// </summary>
        /// <param name="value">
        /// The time value to set duration to.
        /// </param>
        public virtual void SetDuration(TimeSpan value)
        {
            this.usageDuration = value;
        }

        /// <summary>
        /// Starts the usability object.
        /// </summary>
        public virtual void Start()
        {
            if (this.inUse || this.isOffCooldown == false || this.usageLimit == 0)
            {
                var manager = Localization.LocalizationManager.Instance;
                this.events.Report(KerwiluCategory.Other, KerwiluMessageType.Information, manager.Get("MSG_ItemCanNotBeUsedRightNow"));
                return;
            }

            this.inUse = true;
            if (this.cooldownTimeSpan > TimeSpan.Zero)
            {
                this.isOffCooldown = false;
            }

            this.args.ReturnValue = false;
            if (this.StartUsage != null)
            {
                this.StartUsage(this, this.args);
            }

            if (this.args.ReturnValue)
            {
                if (this.usageLimit != int.MinValue && this.usageLimit > 0)
                {
                    this.usageLimit--;
                }

                this.firstUpdate = true;
                this.usageTimeSpan = TimeSpan.Zero;
                this.events.ItemUpdate += this.Update;
            }
            else
            {
                this.inUse = false;
            }
        }

        /// <summary>
        /// Stops the usability.
        /// </summary>
        public virtual void Stop()
        {
            this.inUse = false;
            this.args.ReturnValue = true;
            if (this.StopUsage != null)
            {
                this.StopUsage(this, this.args);
            }

            // if still on cool down don't unhook event yet
            if (this.isOffCooldown)
            {
                this.events.ItemUpdate -= this.Update;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the usability object.
        /// </summary>
        /// <param name="sender">
        /// Typically a reference to the caller.
        /// </param>
        /// <param name="e">
        /// Contains item event data.
        /// </param>
        private void Update(object sender, ItemEventArgs e)
        {
            TimeSpan currentGameTime = e.TotalGameTime;
            if (this.inUse)
            {
                if (this.firstUpdate)
                {
                    this.firstUpdate = false;
                    this.lastTime = currentGameTime;
                    this.lastCooldownTime = currentGameTime;
                }

                this.usageTimeSpan += currentGameTime - this.lastTime;
                this.lastTime = currentGameTime;
                this.remainingTimeOnCooldown = (this.lastCooldownTime + this.cooldownTimeSpan) - currentGameTime;

                if (this.usageTimeSpan >= this.usageDuration)
                {
                    this.inUse = false;
                    this.args.ReturnValue = true;
                    if (this.StopUsage != null)
                    {
                        this.StopUsage(this, this.args);
                    }
                }
            }

            this.remainingTimeOnCooldown = (this.lastCooldownTime + this.cooldownTimeSpan) - currentGameTime;

            if (this.isOffCooldown == false && this.remainingTimeOnCooldown < TimeSpan.Zero)
            {
                this.isOffCooldown = true;
                this.remainingTimeOnCooldown = TimeSpan.Zero;
                this.events.ItemUpdate -= this.Update;
            }
        }

        #endregion
    }
}