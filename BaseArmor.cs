﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System.Collections.Generic;

    using CBX.Common;
    using CBX.Kerwilu.Interfaces;
    using CBX.PropertyModifiers;

    /// <summary>
    /// Inherits from <see cref="Item"/> and provides a base implementation for the <see cref="IArmor{T}"/> type.
    /// </summary>
    /// <typeparam name="TProperty">
    /// The generic type used to determine if an actor passed into the <see cref="Equip"/> or <see cref="UnEquip"/> methods implements <see cref="IProperties{T}"/>.
    /// </typeparam>
    /// <typeparam name="TPropertyType">
    /// The generic type used by <see cref="IProperties{TProperty}.GetProperty{T}"/>.  
    /// </typeparam>
    /// <typeparam name="TSlot">
    /// The generic type used to define the armor slot indexer.
    /// </typeparam>
    /// <remarks>
    /// <para>A inherited object must override the slot property.</para>   
    /// </remarks>    
    public abstract class BaseArmor<TProperty, TPropertyType, TSlot> : Item, IArmor<TSlot>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseArmor{TProperty,TPropertyType,TSlot}"/> class.
        /// </summary>
        protected BaseArmor()
        {
            this.ActorModifiers = new Dictionary<TProperty, IPropertyModifier<TPropertyType>>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the list of property modifiers for specified actor properties.
        /// </summary>
        public Dictionary<TProperty, IPropertyModifier<TPropertyType>> ActorModifiers { get; set; }

        /// <summary>
        /// Gets the slot that the armor fits into.
        /// </summary>
        /// <remarks>This property must be overridden in a derived type.</remarks>
        public abstract TSlot Slot { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Equips the armor onto a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor who will be equipped with the armor.
        /// </param>
        /// <remarks>
        /// If actor or <see cref="ActorModifiers"/> is null this method does nothing.
        /// </remarks>
        public virtual void Equip(IActor actor)
        {
            // do nothing is actor is null
            if (actor == null || this.ActorModifiers == null)
            {
                return;
            }

            // process all modifiers
            foreach (TProperty prop in this.ActorModifiers.Keys)
            {
                IProperty<TPropertyType> value = null;
                //value = actor.Properties.GetValue<IProperty<TPropertyType>>(prop);
                if (actor is IProperties<TProperty>)
                {
                    var tmp = actor as IProperties<TProperty>;
                    value = tmp.GetProperty<IProperty<TPropertyType>>(prop);
                }

                var mod = this.ActorModifiers[prop];
                if (mod != null && value != null)
                {
                    // only add if modifiers list does not already contain the modifier
                    if (!value.Modifiers.Contains(mod))
                    {
                        value.Modifiers.Add(mod);
                    }
                }
            }
        }

        /// <summary>
        /// UnEquips the armor from a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor who will have the armor unequipped.
        /// </param>
        /// <remarks>
        /// If actor or <see cref="ActorModifiers"/> is null this method does nothing.
        /// </remarks>
        public virtual void UnEquip(IActor actor)
        {
            // do nothing is actor is null
            if (actor == null)
            {
                return;
            }

            foreach (TProperty prop in this.ActorModifiers.Keys)
            {
                IProperty<TPropertyType> value = null;
                if (actor is IProperties<TProperty>)
                {
                    var tmp = actor as IProperties<TProperty>;
                    value = tmp.GetProperty<IProperty<TPropertyType>>(prop);
                }

                if (value == null)
                {
                    continue;
                }

                var modifier = this.ActorModifiers[prop];
                if (modifier != null)
                {
                    value.Modifiers.Remove(modifier);
                }
            }

            if (this.Useability != null)
            {
                this.Useability.Stop();
            }
        }

        #endregion
    }
}