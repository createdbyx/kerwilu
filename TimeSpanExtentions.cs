﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace System
{
    using System.Globalization;

    /// <summary>
    /// Provides <see cref="TimeSpan"/> extension methods.
    /// </summary>
    public static class TimeSpanExtentions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Converts a <see cref="TimeSpan"/> type into a formatted string.
        /// </summary>
        /// <param name="time">
        /// The time to convert.
        /// </param>
        /// <returns>
        /// Returns a user friendly cool down string.
        /// </returns>
        public static string ToCooldownString(this TimeSpan time)
        {
            string value = string.Empty;
            if (time.Days > 0)
            {
                value += time.Days.ToString(CultureInfo.InvariantCulture) + " Day" + (time.Days > 1 ? "s" : string.Empty);
            }

            if (time.Hours > 0)
            {
                value += time.Hours.ToString(CultureInfo.InvariantCulture) + " H" + (time.Hours > 1 ? "rs" : "our");
            }

            if (time.Minutes > 0)
            {
                value += time.Minutes.ToString(CultureInfo.InvariantCulture) + " Min" + (time.Minutes > 1 ? "s" : string.Empty);
            }

            if (time.Seconds > 0)
            {
                value += time.Seconds.ToString(CultureInfo.InvariantCulture) + " Sec" + (time.Seconds > 1 ? "s" : string.Empty);
            }

            return value.TrimEnd(' ');
        }

        #endregion
    }
}