﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System;

    using CBX.Kerwilu.Interfaces;

    /// <summary>
    /// A base implementation of the <see cref="IItem"/> interface.
    /// </summary>
    public class Item : IItem
    {
        #region Constants and Fields

        /// <summary>
        /// Holds the value of the description.
        /// </summary>
        private string description;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the category that this item is categorized under.
        /// </summary>
        public virtual string Category { get; set; }

        /// <summary>
        /// Gets or sets the description for the item.
        /// </summary>
        /// <remarks>If the <see cref="Useability"/> property has been set retrieving the description will also include the <see cref="IUseable.GetDescription"/> appended to the end.</remarks>
        public virtual string Description
        {
            get
            {
                return this.description + (this.Useability != null ? this.Useability.GetDescription() : string.Empty);
            }

            set
            {
                this.description = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of this item.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets a unique <see cref="Guid"/> for this item.
        /// </summary>
        public virtual Guid UniqueId
        {
            get
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Gets or sets a <see cref="IUseable"/> implementation.
        /// </summary>
        public virtual IUseable Useability { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Executes this item against the specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor that should be affected by this item.
        /// </param>
        /// <remarks>If actor is null this methods does nothing.</remarks>
        public virtual void Execute(IActor actor)
        {
            if (actor == null)
            {
                return;
            }
        }

        /// <summary>
        /// Uses the item.
        /// </summary>
        /// <remarks>
        /// Calls the <see cref="Usability.Start"/> method. Will only work if the Usability property has been set.
        /// </remarks>
        public virtual void Use()
        {
            if (this.Useability != null)
            {
                this.Useability.Start();
            }
        }

        #endregion
    }
}