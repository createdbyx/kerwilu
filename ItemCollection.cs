﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System.Collections.Generic;

    using CBX.Kerwilu.Interfaces;

    /// <summary>
    /// Provides a collection for <see cref="IItem"/> types.
    /// </summary>
    public class ItemCollection : GenericCollection<IItem>
    {
    }
}