﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    using System;

    using CBX.Kerwilu.Args;

    /// <summary>
    /// Provides a interface for usable items.
    /// </summary>
    public interface IUseable
    {
        #region Public Events

        /// <summary>
        /// Event that gets raised when usage begins.
        /// </summary>
        event EventHandler<UsageEventArgs> StartUsage;

        /// <summary>
        /// Event that gets raised when usage is over.
        /// </summary>
        event EventHandler<UsageEventArgs> StopUsage;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the number of charges.
        /// </summary>
        /// <returns>Returns the number of remaining charges. If the values is int.MinValue then there is no limit on it's use.</returns>
        int Charges { get; }

        /// <summary>
        /// Gets the cool down time before the item can be used again.
        /// </summary>
        TimeSpan Cooldown { get; }

        /// <summary>
        /// Gets the remaining time before the item can be used again.
        /// </summary>
        TimeSpan RemainingTimeOnCooldown { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the description for the usable implementation.
        /// </summary>
        /// <returns>
        /// Returns a description string.
        /// </returns>
        string GetDescription();

        /// <summary>
        /// Sets the number of charges.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        void SetCharges(int value);

        /// <summary>
        /// Sets the cool down time.
        /// </summary>
        /// <param name="value">
        /// The value to set cool down to.
        /// </param>
        void SetCooldown(TimeSpan value);

        /// <summary>
        /// The set duration.
        /// </summary>
        /// <param name="value">
        /// The time value to set duration to.
        /// </param>
        void SetDuration(TimeSpan value);

        /// <summary>
        /// Starts the usability object.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the usability.
        /// </summary>
        void Stop();

        #endregion
    }
}