﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    using System;

    /// <summary>
    /// Provides a IKnowledge interface for knowledge based objects.
    /// </summary>
    public interface IKnowledge
    {
        #region Public Properties

        /// <summary>
        /// Gets the Category where the knowledge is located.
        /// </summary>
        string Category { get; }

        /// <summary>
        /// Gets the description for the knowledge.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Gets the name for the knowledge.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a unique <see cref="Guid"/> for the knowledge.
        /// </summary>
        Guid UniqueId { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Executes the knowledge against a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor that will be the target of the knowledge.
        /// </param>
        void Execute(IActor actor);

        #endregion
    }
}