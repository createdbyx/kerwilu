﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    using CBX.Common;

    public interface IActor
    {
        ItemCollection Items { get; set; }
        KnowledgeCollection Knowledge { get; set; }

        //IValues<string> Properties { get; set; }
        // IProperty<float> GetProperty(string index);
        // void SetProperty(string index, IProperty<float> value);
    }
}
