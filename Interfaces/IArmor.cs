/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    /// <summary>
    /// Provides a IArmor interface for armor.
    /// </summary>
    /// <typeparam name="T">
    /// A generic type used for the armor slot.
    /// </typeparam>
    public interface IArmor<T>
    {
        #region Public Properties

        /// <summary>
        /// Gets the slot that the armor fits into.
        /// </summary>
        T Slot { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Equips the armor onto a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor who will be equipped with the armor.
        /// </param>
        void Equip(IActor actor);

        /// <summary>
        /// UnEquips the armor from a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor who will have the armor unequipped.
        /// </param>
        void UnEquip(IActor actor);

        #endregion
    }
}