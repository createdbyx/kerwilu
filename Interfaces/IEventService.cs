﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    using System;

    using CBX.Kerwilu.Args;

    /// <summary>
    /// Provides a event interface for reporting events.
    /// </summary>
    public interface IEventService
    {
        #region Public Events

        /// <summary>
        /// Provides an event for item specific messages.
        /// </summary>
        event EventHandler<MessageEventArgs> ItemMessages;

        /// <summary>
        /// Provides an event for item specific updates.
        /// </summary>
        event EventHandler<ItemEventArgs> ItemUpdate;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Reports an ItemMessages event.
        /// </summary>
        /// <param name="category">
        /// The category for the event.
        /// </param>
        /// <param name="type">
        /// The type of event that will be reported.
        /// </param>
        /// <param name="message">
        /// The message to include with the event.
        /// </param>
        void Report(KerwiluCategory category, KerwiluMessageType type, string message);

        /// <summary>
        /// Fires an ItemUpdate event.
        /// </summary>
        /// <param name="totalGameTime">
        /// The total game time.
        /// </param>
        /// <param name="elapsedGameTime">
        /// The elapsed game time since the last update.
        /// </param>
        void Update(TimeSpan totalGameTime, TimeSpan elapsedGameTime);

        #endregion
    }
}