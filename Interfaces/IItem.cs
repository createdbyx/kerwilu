﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Interfaces
{
    using System;

    /// <summary>
    /// Provides a interface for items.
    /// </summary>
    public interface IItem
    {
        #region Public Properties

        /// <summary>
        /// Gets the category that the item is categorized under.
        /// </summary>
        string Category { get; }

        /// <summary>
        /// Gets the description for the item.
        /// </summary>
        /// <remarks>Implementers of this interface should check if the <see cref="Useability"/> property has been set when retrieving the description and also include the <see cref="IUseable.GetDescription"/> appended to the end.</remarks>
        string Description { get; }

        /// <summary>
        /// Gets the name of this item.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a unique <see cref="Guid"/> for this item.
        /// </summary>
        Guid UniqueId { get; }

        /// <summary>
        /// Gets or sets a <see cref="IUseable"/> implementation.
        /// </summary>
        IUseable Useability { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Executes this item against the specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor that should be affected by this item.
        /// </param>                                                         
        void Execute(IActor actor);

        /// <summary>
        /// Uses the item.
        /// </summary>
        void Use();

        #endregion                                 
    }
}