/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    /// <summary>
    /// Kerwilu category types.
    /// </summary>
    public enum KerwiluCategory
    {
        /// <summary>
        /// Some other category.
        /// </summary>
        Other, 

        /// <summary>
        /// The item category.
        /// </summary>
        Item
    }
}