/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System;

    using CBX.Kerwilu.Interfaces;

    /// <summary>
    /// The usable knowledge.
    /// </summary>
    public abstract class UsableKnowledge : UsableObject, IKnowledge
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UsableKnowledge"/> class.
        /// </summary>
        /// <param name="service">
        /// A reference to a <see cref="IEventService"/> service type.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If service is null.
        /// </exception>
        protected UsableKnowledge(IEventService service)
            : base(service)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the category that this usable knowledge object belongs to.
        /// </summary>
        public abstract string Category { get; }

        /// <summary>
        /// Gets the description for the knowledge.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// Gets the name of this knowledge.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Gets a unique <see cref="Guid"/> for the knowledge.
        /// </summary>
        public abstract Guid UniqueId { get; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Executes the knowledge against a specified actor.
        /// </summary>
        /// <param name="actor">
        /// The actor that will be the target of the knowledge.
        /// </param>
        public abstract void Execute(IActor actor);

        #endregion
    }
}