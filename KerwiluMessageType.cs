/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    /// <summary>
    /// Kerwilu message types.
    /// </summary>
    public enum KerwiluMessageType
    {
        /// <summary>
        /// The message type is information.
        /// </summary>
        Information, 

        /// <summary>
        /// The message type is an error.
        /// </summary>
        Error, 

        /// <summary>
        /// The message type is a warning.
        /// </summary>
        Warrning, 
    }
}