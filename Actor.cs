﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu
{
    using System.Collections.Generic;

    using CBX.Kerwilu.Interfaces;

    /// <summary>
    /// Provides a base class for and Actor type.
    /// </summary>
    /// <typeparam name="T">
    /// A generic type that is used as the armor index.
    /// </typeparam>
    public class Actor<T> : IActor   
    {
        #region Constants and Fields

        /// <summary>
        /// Holds a series of armor items.
        /// </summary>
        private readonly Dictionary<T, IArmor<T>> armorItems;

        /// <summary>
        /// Holds the list of items.
        /// </summary>
        private ItemCollection items;

        /// <summary>
        /// Holds the collection of knowledge entries.
        /// </summary>
        private KnowledgeCollection knowledge;

        /// <summary>
        /// Used to store the actors name.
        /// </summary>
        private string name;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Actor{T}"/> class.
        /// </summary>
        public Actor()
        {
            this.items = new ItemCollection();
            this.knowledge = new KnowledgeCollection();

            this.name = string.Empty;

            this.armorItems = new Dictionary<T, IArmor<T>>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the collection of items.
        /// </summary>
        public virtual ItemCollection Items
        {
            get
            {
                return this.items;
            }

            set
            {
                this.items = value;
            }
        }

        /// <summary>
        /// Gets or sets the collection of knowledge items.
        /// </summary>
        public virtual KnowledgeCollection Knowledge
        {
            get
            {
                return this.knowledge;
            }

            set
            {
                this.knowledge = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the actor.
        /// </summary>
        public virtual string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Equips a armor item.
        /// </summary>
        /// <param name="item">
        /// The item that will be equipped.
        /// </param>
        /// <remarks>
        /// If item is null this method does nothing.
        /// </remarks>
        public virtual void EquipArmorItem(IArmor<T> item)
        {
            if (item == null)
            {
                return;
            }

            if (this.armorItems[item.Slot] != null)
            {
                this.UnEquipArmorItem(item.Slot);
            }

            this.armorItems[item.Slot] = item;
            item.Equip(this);
        }

        /// <summary>
        /// The get armor item.
        /// </summary>
        /// <param name="index">
        /// The armor index.
        /// </param>
        /// <returns>
        /// Returns a reference to a armor item at the specified index.
        /// </returns>
        public virtual IArmor<T> GetArmorItem(T index)
        {
            return this.armorItems[index];
        }

        /// <summary>
        /// Un-equips the armor item at the specified slot.
        /// </summary>
        /// <param name="index">
        /// The armor index.
        /// </param>
        public virtual void UnEquipArmorItem(T index)
        {
            IArmor<T> item = this.armorItems[index];
            if (item == null)
            {
                return;
            }

            item.UnEquip(this);
            this.armorItems[index] = null;
        }

        #endregion
    }
}