/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Args
{
    using System;

    /// <summary>
    /// The usage event args.
    /// </summary>
    public class UsageEventArgs : EventArgs
    {
        #region Constants and Fields

        /// <summary>
        /// The return value.
        /// </summary>
        private bool returnValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UsageEventArgs"/> class.
        /// </summary>
        public UsageEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsageEventArgs"/> class.
        /// </summary>
        /// <param name="returnvalue">
        /// The returnvalue.
        /// </param>
        public UsageEventArgs(bool returnvalue)
        {
            this.returnValue = returnvalue;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether ReturnValue.
        /// </summary>
        public bool ReturnValue
        {
            get
            {
                return this.returnValue;
            }

            set
            {
                this.returnValue = value;
            }
        }

        #endregion
    }
}