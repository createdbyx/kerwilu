/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Args
{
    using System;

    /// <summary>
    /// The item event arguments containing time stamp information.
    /// </summary>
    public class ItemEventArgs : EventArgs
    {
        #region Constants and Fields

        /// <summary>
        /// Holds the elapsed game time value.
        /// </summary>
        private TimeSpan elapsedGameTime;

        /// <summary>
        /// Holds the total game time value.
        /// </summary>
        private TimeSpan totalGameTime;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemEventArgs"/> class.
        /// </summary>
        public ItemEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemEventArgs"/> class. 
        /// </summary>
        /// <param name="totalGameTime">
        /// Value representing the total game time.
        /// </param>
        /// <param name="elapsedGameTime">
        /// Value representing the elapsed game time since last update etc.
        /// </param>
        public ItemEventArgs(TimeSpan totalGameTime, TimeSpan elapsedGameTime)
        {
            this.totalGameTime = totalGameTime;
            this.elapsedGameTime = elapsedGameTime;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the elapsed game time value.
        /// </summary>
        public virtual TimeSpan ElapsedGameTime
        {
            get
            {
                return this.elapsedGameTime;
            }

            set
            {
                this.elapsedGameTime = value;
            }
        }

        /// <summary>
        /// Gets or sets the total game time value.
        /// </summary>
        public virtual TimeSpan TotalGameTime
        {
            get
            {
                return this.totalGameTime;
            }

            set
            {
                this.totalGameTime = value;
            }
        }

        #endregion
    }
}