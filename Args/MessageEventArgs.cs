/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace CBX.Kerwilu.Args
{
    using System;

    /// <summary>
    /// Provides a message arguments type.
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEventArgs"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public MessageEventArgs(string message)
        {
            this.Category = KerwiluCategory.Other;
            this.MessageType = KerwiluMessageType.Information;
            this.Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEventArgs"/> class.
        /// </summary>
        /// <param name="type">
        /// The type of message.
        /// </param>
        public MessageEventArgs(KerwiluMessageType type)
            : this()
        {
            this.MessageType = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEventArgs"/> class.
        /// </summary>
        /// <param name="type">
        /// The type of message.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public MessageEventArgs(KerwiluMessageType type, string message)
            : this(type)
        {
            this.Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageEventArgs"/> class.
        /// </summary>
        public MessageEventArgs()
        {
            this.Category = KerwiluCategory.Other;
            this.Message = string.Empty;
            this.MessageType = KerwiluMessageType.Information;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the message category.
        /// </summary>
        public KerwiluCategory Category { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the message type.
        /// </summary>
        public KerwiluMessageType MessageType { get; set; }

        #endregion
    }
}